#ifndef FB_RENDERER_H
#define FB_RENDERER_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <string>

namespace renderer {

    /**
    *   Configure the call thread context for the render.
    *   Call this function after creating the context and before rendering the frame.
    *   If successful return true, otherwise return false and write log to <errorLog> (if <errorLog> is not null)
    **/
    bool onCreate(int width, int height, std::string* errorLog = nullptr);
    /**
    *   Reconfigure the renderer to a new resolution.
    *   Call this method when changing the window resolution.
    **/
    void onResize(GLFWwindow* window, int width, int height);
    /**
    *   Render a frame.
    **/
    void onDraw(float u_time);
    /**
    *   Reset all settings and release resources.
    *   For further rendering after calling this method, you need to reconfigure the context (renderer :: OnCreate)
    **/
    void onDestroy(GLFWwindow* window);

}

#endif // FB_RENDERER_H
