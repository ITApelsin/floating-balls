#include "fb_renderer.h"
#include <iostream>

#define DEFAULT_WIDTH 800
#define DEFAULT_HEIGHT 600

int main(void)
{
    using namespace renderer;
    using namespace std;

    if (!glfwInit())
        return -1;

    GLFWwindow* window;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(DEFAULT_WIDTH, DEFAULT_HEIGHT, APP_NAME, NULL, NULL);
    glfwMakeContextCurrent(window);

    std::string errorLog;

    // Check context initialization, initialize pointers to OpenGL functions, and configure context
    if (!window || !gladLoadGLLoader((GLADloadproc) glfwGetProcAddress) || !onCreate(DEFAULT_WIDTH, DEFAULT_HEIGHT, &errorLog))
    {
        // In case of an error, print it to the console and close the application
        cout << "Initialization error " << errorLog << endl;
        glfwTerminate();
        return -1;
    }

    glfwSetWindowSizeCallback(window, onResize);
    glfwSetWindowCloseCallback(window, onDestroy);

    const double maxFPS = 60.0;
    const double maxPeriod = 1.0 / maxFPS;
    double lastTime = 0.0;

    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        double time = glfwGetTime();

        if( time - lastTime >= maxPeriod )
        {
            lastTime = time;
            onDraw(static_cast<float>(time));
            glfwSwapBuffers(window);
        }

    }

    glfwTerminate();
    return 0;
}
