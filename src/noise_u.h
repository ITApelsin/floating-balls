#ifndef FB_NOISE_UTILS_H
#define FB_NOISE_UTILS_H

#include "shader_u.h"

namespace noise_utils
{
    /**
    *   Initialize a noise generator
    *   If successful return true, otherwise return flase and write log to <errorLog> (if <errorLog> is not null)
    **/
    bool initialize(std::string *errorLog = nullptr);
    /**
    *   Generate a square noise texture with the <texture_size> side for the time value <u_time> in the <framebuffer> framebuffer.
    *   Before calling, you must initialize the generator (noise_utils :: initialize)
    **/
    void genNoise(float u_time, GLuint framebuffer, int texture_size);
}

#endif // FB_NOISE_UTILS_H
