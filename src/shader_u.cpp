#include "shader_u.h"
#include <fstream>
#include <sstream>

namespace shader_utils
{
    bool readFile(std::string& dest, const char* path)
    {
        std::stringstream strs;
        std::fstream file(path);
        if (file.is_open())
        {
            std::string line;
            while (std::getline(file, line))
            {
                strs << line << '\n';
            }
            dest = strs.str();
            file.close();
            return true;
        }
        return false;
    }

    bool compileShaderFromSource(GLuint &dest, GLenum shaderType, const char* source, std::string *errorLog)
    {
        GLuint shader = glCreateShader(shaderType);
        glShaderSource(shader, 1, &source, NULL);
        glCompileShader(shader);
        GLint isCompiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE)
        {
            if (errorLog)
            {
                GLint maxLength = 0;
                glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
                char infoLog[maxLength];
                glGetShaderInfoLog(shader, maxLength, &maxLength, infoLog);
                *errorLog = std::string(infoLog);
            }
            glDeleteShader(shader);
            return false;
        }
        dest = shader;
        return true;
    }

    bool compileShaderFromFile(GLuint &dest, GLenum shaderType, const char *path, std::string *errorLog)
    {
        std::string shaderSource;
        if (!readFile(shaderSource, path))
        {
            if (errorLog)
            {
                *errorLog = "IO_ERROR: cant read file";
            }
            return false;
        }
        return compileShaderFromSource(dest, shaderType, shaderSource.c_str(), errorLog);
    }

    GLProgramBuilder* GLProgramBuilder::reset()
    {
        setShaders(nullptr, nullptr);
        setAutoDetaching();
        setTransformFeedback(0, nullptr);
        return this;
    }

    GLProgramBuilder* GLProgramBuilder::setShaders(GLuint * const vertex_shader, GLuint * const fragments_shader)
    {
        vertexShader = vertex_shader;
        fragmentShader = fragments_shader;
        return this;
    }

    GLProgramBuilder* GLProgramBuilder::setAutoDetaching(bool autoDetaching)
    {
        this->autoDetaching = autoDetaching;
        return this;
    }

    GLProgramBuilder* GLProgramBuilder::setTransformFeedback(GLsizei count, const char** varyings, GLenum bufferMode)
    {
        transformFeedback.count = count;
        transformFeedback.varyings = varyings;
        transformFeedback.bufferMode = bufferMode;
        return this;
    }

    bool GLProgramBuilder::link(GLuint &dest, std::string *errorLog)
    {
        if (!vertexShader || !fragmentShader || (transformFeedback.count > 0 && !transformFeedback.varyings))
        {
            if (errorLog)
            {
                *errorLog = "invalid state: vertex shader, fragmentss shader or transform feedback varyings is null";
            }
            return false;
        }
        GLuint program = glCreateProgram();
        glAttachShader(program, *vertexShader);
        glAttachShader(program, *fragmentShader);
        if (transformFeedback.count > 0)
        {
            glTransformFeedbackVaryings(program, transformFeedback.count, transformFeedback.varyings, transformFeedback.bufferMode);
        }
        glLinkProgram(program);
        if (autoDetaching)
        {
            glDetachShader(program, *vertexShader);
            glDetachShader(program, *fragmentShader);
        }
        GLint isLinked = 0;
        glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
        if (isLinked == GL_FALSE)
        {
            if (errorLog != nullptr) {
                GLint maxLength = 0;
                glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);
                char _infoLogBuff[maxLength];
                glGetProgramInfoLog(program, maxLength, &maxLength, _infoLogBuff);
                *errorLog = std::string(_infoLogBuff);
            }
            glDeleteProgram(dest);
            return false;
        }
        dest = program;
        return true;
    }
}
