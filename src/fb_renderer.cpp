#include <memory>
#include <algorithm>
#include <cstddef>
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>
#include "fb_renderer.h"
#include "noise_u.h"

#define POSITION_LOCATION 0
#define MOVE_VECTOR_LOCATION 1
#define COLOR_LOCATION 2
#define VELOCITY_LOCATION 3
#define POINT_SIZE_LOCATION 4
#define BIRTHTIME_LOCATION 5
#define LIFETIME_LOCATION 6
#define U_TIME_LOCATION 0

#define DEFAULT_CANVAS_WIDTH 1024
#define DEFAULT_CANVAS_HEIGHT 1024
#define TEXTURE_SIZE 512

namespace renderer
{
    using namespace std;
    using namespace glm;
    using namespace shader_utils;

    class GLRenderer
    {
    public:
        virtual ~GLRenderer() = default;
        virtual void onResize(int width, int height) = 0;
        virtual void onDraw(float u_time) = 0;
    };

    thread_local unique_ptr<GLRenderer> glRenderer;

    #pragma pack(push, 1)
    struct Ball
    {
        vec2 position;
        vec2 moveVector;
        vec3 color;
        float velosity;
        float pointSize;
        float birthtime;
        float lifetime;
        /**
        *   Calculates the remaining particle lifetime at time <u_time>
        **/
        float getTimeleft(float u_time);
        /**
        *   Calculates the collision between the current particle and <ball>,
        *   given the scaling of <u_scale> at time <u_time>.
        *   If at time <u_time> one of the particles or both is considered dead or no collision was detected, returns false, 
		*	otherwise returns true.
        **/
        bool checkCollision(Ball& ball, float u_scale, float u_time);
    };
    #pragma pack(pop)

    class FBRenderer : public GLRenderer
    {
        friend bool onCreate(int width, int height, string* errorLog);

        const GLuint RENDER_PROGRAM;
        const GLuint GPGPU_PROGRAM;

        GLuint offscreenFBO;
        GLuint noiseMap;

        GLuint vbo[2];
        GLuint vao;

        vec3 background_color;
        int numParticles;
        int width;
        int height;
        float u_scale;

        FBRenderer(GLuint render_program,
                   GLuint gpgpu_program,
                   int width,
                   int height,
                   vec3 background_color = vec3(0.5, 0.5, 0.5),
                   float emmisionRate = 0.08,
                   int numParticles = 50);
        /**
        *   Swaps vertex buffers.
        **/
        void pongBuff(void);
        /**
        *   Calculates collisions between particles and changes the moveVector for <nParticles> particles in the <ballsBuffer> buffer
        **/
        void checkCollision(Ball * const ballsBuffer, int numParticles);
    public:
        ~FBRenderer();
        void onResize(int width, int height) override;
        void onDraw(float u_time) override;
    };

    /**
    *   Returns the distance between point <a> and point <b>
    **/
    int fastDist(vec2 a, vec2 b)
    {
        int x = static_cast<int>(round(abs(a.x - b.x)));
        int y = static_cast<int>(round(abs(a.y - b.y)));
        int minValue = std::min(x,y);
        return x + y - (minValue >> 1) - (minValue>>2) + (minValue >> 4);
    }

    float Ball::getTimeleft(float u_time)
    {
        return birthtime + lifetime - u_time;
    }

    bool Ball::checkCollision(Ball& ball, float u_scale, float u_time)
    {
        const float endLife = 0.3;
        if (this->getTimeleft(u_time) > endLife && ball.getTimeleft(u_time) > endLife)
        {
            int dist = fastDist(this->position, ball.position);
            return (static_cast<float>(dist) - (this->pointSize*u_scale)/2 - (ball.pointSize * u_scale)/2) < 0;
        } else return false;
    }

    FBRenderer::FBRenderer(GLuint gpgpu_program,
                           GLuint render_program,
                           int width,
                           int height,
                           vec3 background_color,
                           float emmisionRate ,
                           int numParticles) :
        GPGPU_PROGRAM(gpgpu_program),
        RENDER_PROGRAM(render_program),
        background_color(background_color),
        numParticles(numParticles)
    {
        GLint defaultFBO;
        glGetIntegerv(GL_FRAMEBUFFER_BINDING, &defaultFBO);

        // Create and configure a texture object for noise texture
        glActiveTexture(GL_TEXTURE1);
        glGenTextures(1, &noiseMap);
        glBindTexture(GL_TEXTURE_2D, noiseMap);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, TEXTURE_SIZE, TEXTURE_SIZE, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        //  Create and configure an offscreen framebuffer to generate noise texture
        glGenFramebuffers(1, &offscreenFBO);
        glBindFramebuffer(GL_FRAMEBUFFER, offscreenFBO);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, noiseMap, 0);

        glBindFramebuffer(GL_FRAMEBUFFER, defaultFBO);

        //  Allocate space for data buffers
        glGenBuffers(2, vbo);
        for (size_t i = 0; i < 2; i++)
        {
            glBindBuffer(GL_ARRAY_BUFFER, vbo[i]);
            glBufferData(GL_ARRAY_BUFFER, sizeof(Ball)* numParticles, NULL, GL_STREAM_DRAW);
        }
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        //  Configure the vertex array object object
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glVertexAttribFormat(POSITION_LOCATION, 2, GL_FLOAT, GL_FALSE, offsetof(Ball, position));
        glVertexAttribFormat(MOVE_VECTOR_LOCATION, 2, GL_FLOAT, GL_FALSE, offsetof(Ball, moveVector));
        glVertexAttribFormat(COLOR_LOCATION, 3, GL_FLOAT, GL_FALSE, offsetof(Ball, color));
        glVertexAttribFormat(VELOCITY_LOCATION, 1, GL_FLOAT, GL_FALSE, offsetof(Ball, velosity));
        glVertexAttribFormat(POINT_SIZE_LOCATION, 1, GL_FLOAT, GL_FALSE, offsetof(Ball, pointSize));
        glVertexAttribFormat(BIRTHTIME_LOCATION, 1, GL_FLOAT, GL_FALSE, offsetof(Ball, birthtime));
        glVertexAttribFormat(LIFETIME_LOCATION, 1, GL_FLOAT, GL_FALSE, offsetof(Ball, lifetime));
        const GLuint attribLocations[]
        {
            POSITION_LOCATION,
            MOVE_VECTOR_LOCATION,
            COLOR_LOCATION,
            VELOCITY_LOCATION,
            POINT_SIZE_LOCATION,
            BIRTHTIME_LOCATION,
            LIFETIME_LOCATION
        };
        for (size_t i = 0; i < 7; i++) {
            glEnableVertexAttribArray(attribLocations[i]);
            glVertexAttribBinding(attribLocations[i], 0);
        }

        pongBuff();

        //  Set constants
        glUseProgram(GPGPU_PROGRAM);
        glUniform1i(glGetUniformLocation(GPGPU_PROGRAM, "s_noiseMap"), 1);
        glUniform1i(glGetUniformLocation(GPGPU_PROGRAM, "u_numParticles"), numParticles);
        glUniform1f(glGetUniformLocation(GPGPU_PROGRAM, "u_emmissionRate"), emmisionRate);
        glClearColor(background_color.r, background_color.g, background_color.b, 1.0);
        onResize(width, height);
    }

    FBRenderer::~FBRenderer()
    {
        //	Release of resources
        glDeleteProgram(GPGPU_PROGRAM);
        glDeleteProgram(RENDER_PROGRAM);
        glDeleteFramebuffers(1, &offscreenFBO);
        glDeleteTextures(1, &noiseMap);
        glDeleteBuffers(2, vbo);
        glDeleteVertexArrays(1, &vao);
    }

    void FBRenderer::onResize(int width, int height)
    {
        // Adapt the context to the new resolution of screen
        this->width = width;
        this->height = height;
        this->u_scale = std::min(static_cast<float>(width)/DEFAULT_CANVAS_WIDTH, static_cast<float>(height)/DEFAULT_CANVAS_HEIGHT);
        float halfWidth = static_cast<float>(width)/2;
        float halfHeight = static_cast<float>(height)/2;
        mat4 projectionMat = glm::ortho(-halfWidth, halfWidth, -halfHeight, halfHeight);

        glUseProgram(GPGPU_PROGRAM);
        glUniform1f(glGetUniformLocation(GPGPU_PROGRAM, "u_scale"), u_scale);
        glUniform2f(glGetUniformLocation(GPGPU_PROGRAM, "u_canvas"), static_cast<float>(width), static_cast<float>(height));

        glUseProgram(RENDER_PROGRAM);
        glUniform1f(glGetUniformLocation(RENDER_PROGRAM, "u_scale"), u_scale);
        glUniformMatrix4fv(glGetUniformLocation(RENDER_PROGRAM, "u_projectionMat"), 1, GL_FALSE, glm::value_ptr(projectionMat));

        glViewport(0,0, width, height);
    }

    void FBRenderer::pongBuff()
    {
        std::swap(vbo[0], vbo[1]);
        glBindVertexBuffer(0, vbo[0], 0, sizeof(Ball));
    }

    void FBRenderer::onDraw(float u_time)
    {
        GLsync fencSync;

        //  generate noise
        noise_utils::genNoise(u_time, offscreenFBO, TEXTURE_SIZE);
        glGenerateMipmap(GL_TEXTURE_2D); // �.�. ���������� ���� 1 �� ��� ��� �������, ���������� mipmap ��� �������� ����

        //  Create particles, move them and calculate collisions with static geometry (screen boundaries) on the GPU
        glBindVertexArray(vao);
        glUseProgram(GPGPU_PROGRAM);
        glUniform1f(U_TIME_LOCATION, u_time);
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo[1]);
        glEnable(GL_RASTERIZER_DISCARD);
        glBeginTransformFeedback(GL_POINTS);
            glDrawArrays(GL_POINTS, 0, numParticles);
        glEndTransformFeedback();
        fencSync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
        glDisable(GL_RASTERIZER_DISCARD);

        //  swap vertex buffers
        pongBuff();

        //  Wait for the completion of the transform feedback and render the frame
        glUseProgram(RENDER_PROGRAM);
        glUniform1f(U_TIME_LOCATION, u_time);
        glClear(GL_COLOR_BUFFER_BIT);
        glWaitSync(fencSync, 0, GL_TIMEOUT_IGNORED);
        glDrawArrays(GL_POINTS, 0, numParticles);

        //  Map the active vertex buffer to the application memory and calculate collisions with dynamic geometry (balls) on the CPU
        glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        Ball* bBuffer = reinterpret_cast<Ball*>(glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE));
            for (size_t i = 0; i < numParticles; i++)
            {
                Ball& a = bBuffer[i];
                for (size_t j = i+1; j < numParticles; j++)
                {
                    Ball& b = bBuffer[j];
                    if (a.checkCollision(b, u_scale, u_time))
                    {
                        vec2 v1 = vec2(b.position.x - a.position.x,
                                       b.position.y - a.position.y);
                        vec2 v2 = -v1;
                        a.moveVector = normalize(a.moveVector + v2);
                        b.moveVector = normalize(b.moveVector + v1);
                        std::swap(a.velosity, b.velosity);
                    }
                }
            }
        glUnmapBuffer(GL_ARRAY_BUFFER);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(0);
        glDeleteSync(fencSync);
    }

    bool onCreate(int width, int height, string* errorLog)
    {
        // Initialize the noise generator
        if (!noise_utils::initialize(errorLog))
        {
            return false;
        }
        GLuint gpgpu_vertexShader, gpgpu_fragmentShader;
        GLuint renderp_vertexShader, renderp_fragmentShader;
        // Compile Shaders
        if (compileShaderFromFile(gpgpu_vertexShader, GL_VERTEX_SHADER, "shaders/gpgpu_vs.glsl", errorLog) &&
                compileShaderFromFile(gpgpu_fragmentShader, GL_FRAGMENT_SHADER, "shaders/gpgpu_fs.glsl", errorLog)&&
                compileShaderFromFile(renderp_vertexShader, GL_VERTEX_SHADER, "shaders/renderp_vs.glsl", errorLog) &&
                compileShaderFromFile(renderp_fragmentShader, GL_FRAGMENT_SHADER, "shaders/renderp_fs.glsl", errorLog))
        {
            // If the shader compilation is successful, link the shader programs
            GLuint gpgpu_program;
            GLuint render_program;

            const char* transformFeedbackVaryings[] =
            {
                "v_position",
                "v_moveVector",
                "v_color",
                "v_velocity",
                "v_pointSize",
                "v_birthtime",
                "v_lifetime"
            };
            GLProgramBuilder pBuilder;
            if (pBuilder.setShaders(&gpgpu_vertexShader, &gpgpu_fragmentShader)->setTransformFeedback(7, transformFeedbackVaryings)->
                    link(gpgpu_program, errorLog) &&
                    pBuilder.reset()->setShaders(&renderp_vertexShader, &renderp_fragmentShader)->link(render_program, errorLog))
            {
                // If the shader program linking is successfully then initialize the Renderer and configure the context
                glRenderer.reset(new FBRenderer(gpgpu_program, render_program, width, height));

                glDisable(GL_DEPTH_TEST);
                glDisable(GL_STENCIL_TEST);
                glEnable(GL_PROGRAM_POINT_SIZE);
                glEnable(GL_BLEND);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            }
            else
            {
                if (gpgpu_program)
                    glDeleteProgram(gpgpu_program);
                if (render_program)
                    glDeleteProgram(render_program);
            }

        }
        // Remove the shaders, we don�t need them anymore
        if (gpgpu_vertexShader)
            glDeleteShader(gpgpu_vertexShader);
        if (gpgpu_fragmentShader)
            glDeleteShader(gpgpu_fragmentShader);
        if (renderp_vertexShader)
            glDeleteShader(renderp_vertexShader);
        if (renderp_fragmentShader)
            glDeleteShader(renderp_fragmentShader);
       return glRenderer != nullptr;
    }

    void onResize(GLFWwindow* window, int width, int height)
    {
        if (glRenderer) glRenderer->onResize(width, height);
    }

    void onDraw(float u_time)
    {
        if (glRenderer) glRenderer->onDraw(u_time);
    }

    void onDestroy(GLFWwindow* window)
    {
        glRenderer.reset();
    }
}
