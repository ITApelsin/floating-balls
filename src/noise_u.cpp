#include <memory>
#include "noise_u.h"

#define V_POSITION_LOC 0
#define U_TIME_LOCACTION 0

namespace noise_utils
{
    using namespace std;
    using namespace shader_utils;


    class NoiseGenerator
    {

        struct GLState
        {
            GLint drawFBO;
            GLint program;
            GLint vao;
            GLint viewportParams[4];
            GLfloat clearColor[4];
            GLboolean depthTest, stencilTest;
        };

        friend bool initialize(string *errorLog);
        GLuint nProgram;
        GLuint vbo;
        GLuint vao;
        GLState savedGLState;
        NoiseGenerator(GLuint _program, GLuint _vbo, GLuint _vao);
        /**
        *   Save current OpenGL state to <savedGLState>
        **/
        void pushGLState();
        /**
        *   Restore OpenGL state from <savedGLState> 
        **/
        void popGLState();
    public:
        ~NoiseGenerator();
        void genNoise(float u_time, GLuint framebuffer, int texture_size);
    };

    thread_local unique_ptr<NoiseGenerator> nGen;

    NoiseGenerator::NoiseGenerator(GLuint _program, GLuint _vbo, GLuint _vao):
        nProgram(_program), vbo(_vbo), vao(_vao) {}

    NoiseGenerator::~NoiseGenerator()
    {
        glDeleteProgram(nProgram);
        glDeleteBuffers(1, &vbo);
        glDeleteVertexArrays(1, &vao);
    }

    void NoiseGenerator::pushGLState()
    {
        glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &savedGLState.drawFBO);
        glGetIntegerv(GL_CURRENT_PROGRAM, &savedGLState.program);
        glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &savedGLState.vao);
        glGetFloatv(GL_COLOR_CLEAR_VALUE, savedGLState.clearColor);
        glGetIntegerv(GL_VIEWPORT, savedGLState.viewportParams);
        glGetBooleanv(GL_DEPTH_TEST, &savedGLState.depthTest);
        glGetBooleanv(GL_STENCIL_TEST, &savedGLState.stencilTest);
    }

    void NoiseGenerator::popGLState()
    {
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, savedGLState.drawFBO);
        glUseProgram(savedGLState.program);
        glBindVertexArray(savedGLState.vao);
        glClearColor(savedGLState.clearColor[0],
                     savedGLState.clearColor[1],
                     savedGLState.clearColor[2],
                     savedGLState.clearColor[3]);
        glViewport(savedGLState.viewportParams[0],
                   savedGLState.viewportParams[1],
                   savedGLState.viewportParams[2],
                   savedGLState.viewportParams[3]);
        if (savedGLState.depthTest) glEnable(GL_DEPTH_TEST); else glDisable(GL_DEPTH_TEST);
        if (savedGLState.stencilTest) glEnable(GL_STENCIL_TEST); else glDisable(GL_STENCIL_TEST);
    }

    void NoiseGenerator::genNoise(float u_time, GLuint framebuffer, int texture_size)
    {
        pushGLState();
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, framebuffer);
        glUseProgram(nProgram);
        glUniform1f(U_TIME_LOCACTION, u_time);
        glBindVertexArray(vao);
        glViewport(0,0, texture_size, texture_size);
        glClearColor(0.,0.,0.,1.);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_STENCIL_TEST);
        glClear(GL_COLOR_BUFFER_BIT);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        popGLState();
    }

    bool initialize(string *errorLog)
    {
        GLuint vertexShader;
        GLuint fragmentShader;
        if (compileShaderFromFile(vertexShader, GL_VERTEX_SHADER, "shaders/noisep_vs.glsl", errorLog) &&
                compileShaderFromFile(fragmentShader, GL_FRAGMENT_SHADER, "shaders/noisep_fs.glsl", errorLog))
        {
            // if compile shaders successful
            GLuint program;
            GLProgramBuilder pBuilder;
            if (pBuilder.setShaders(&vertexShader, &fragmentShader)->link(program, errorLog))
            {
                // if link program successful

                // vertices
                //  v0---v3
                //  | \   |
                //  |   \ |
                //  v1---v2
                GLuint vbo;
                GLuint vao;
                float vboData[] =
                {
                    -1.0f,  1.0f,
                    -1.0f, -1.0f,
                     1.0f, -1.0f,
                     1.0f,  1.0f
                };
                glGenBuffers(1, &vbo);
                glGenVertexArrays(1, &vao);

                glBindVertexArray(vao);
                glBindBuffer(GL_ARRAY_BUFFER, vbo);
                glBufferData(GL_ARRAY_BUFFER, sizeof(float)*8, vboData, GL_STATIC_DRAW);
                glEnableVertexAttribArray(V_POSITION_LOC);
                glVertexAttribPointer(V_POSITION_LOC, 2, GL_FLOAT, GL_FALSE, 0, 0);
                glBindVertexArray(0);
                glBindBuffer(GL_ARRAY_BUFFER, 0);
                nGen.reset(new NoiseGenerator(program, vbo, vao));
            }
        }
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
        return nGen != nullptr;
    }

    void genNoise(float u_time, GLuint framebuffer, int texture_size)
    {
        if (nGen) nGen->genNoise(u_time, framebuffer, texture_size);
    }
}
