#version 430 core

#define U_TIME_LOCATION 0
#define POSITION_LOCATION 0
#define MOVE_VECTOR_LOCATION 1
#define COLOR_LOCATION 2
#define VELOCITY_LOCATION 3
#define POINT_SIZE_LOCATION 4
#define BIRTHTIME_LOCATION 5
#define LIFETIME_LOCATION 6

const float zero = 0.0f;
const float one = 1.0f;

layout(location = U_TIME_LOCATION) uniform float u_time;
uniform float u_scale;
uniform mat4 u_projectionMat;

layout(location = POSITION_LOCATION) 	in vec2 position;
layout(location = MOVE_VECTOR_LOCATION) in vec2 moveVector;
layout(location = COLOR_LOCATION) 		in vec3 color;
layout(location = VELOCITY_LOCATION) 	in float velocity;
layout(location = POINT_SIZE_LOCATION) 	in float pointSize;
layout(location = BIRTHTIME_LOCATION) 	in float birthtime;
layout(location = LIFETIME_LOCATION) 	in float lifetime;

flat out vec4 v_color;

void main() {
	float timeleft = birthtime + lifetime - u_time;
	float alpha = max(min(min(lifetime - timeleft, timeleft), one), zero);
	v_color = vec4(color, alpha);
	gl_PointSize = pointSize * u_scale;
	gl_Position = u_projectionMat * vec4(position, one, one);
}