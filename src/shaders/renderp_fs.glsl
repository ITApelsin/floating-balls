#version 430 core

precision mediump float;

const float zero = 0.0f;
const float one = 1.0f;
const float two = 2.0f;

flat in vec4 v_color;

out vec4 color;

float alphaSmooth(in float R, in float dist) {
	return smoothstep(R, R-0.04f, dist);
}

void main() {
	vec2 point = gl_PointCoord*two-one;
	float dist = sqrt(dot(point, point));
	if ((dist <= one) && (v_color.a > zero)) {
		color = vec4(v_color.rgb, v_color.a * alphaSmooth(one, dist));
	} else {
		discard;
	}
}