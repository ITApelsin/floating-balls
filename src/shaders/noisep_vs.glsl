#version 430 core
const float one =1.0f;
layout(location = 0) in vec2 v_position;
void main() {
	gl_Position = vec4(v_position, one, one);
}