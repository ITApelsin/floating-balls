#version 430 core

#define U_TIME_LOCATION 0
#define POSITION_LOCATION 0
#define MOVE_VECTOR_LOCATION 1
#define COLOR_LOCATION 2
#define VELOCITY_LOCATION 3
#define POINT_SIZE_LOCATION 4
#define BIRTHTIME_LOCATION 5
#define LIFETIME_LOCATION 6

const float zero = 0.0f;
const float one = 1.0f;
const float two = 2.0f;
const float min_lifetime = 5.0f;
const float range_lifetime = 15.0f;
const float min_size = 100.0f;
const float range_size = 100.0f;
const float min_velosity = 1.0f;
const float range_velosity = 5.0f;

layout(location = U_TIME_LOCATION) uniform float u_time;
uniform float u_scale;
uniform int u_numParticles;
uniform float u_emmissionRate;
uniform vec2 u_canvas;

uniform sampler2D s_noiseMap;

layout(location = POSITION_LOCATION) 	in vec2 position;
layout(location = MOVE_VECTOR_LOCATION) in vec2 moveVector;
layout(location = COLOR_LOCATION) 		in vec3 color;
layout(location = VELOCITY_LOCATION) 	in float velocity;
layout(location = POINT_SIZE_LOCATION) 	in float pointSize;
layout(location = BIRTHTIME_LOCATION) 	in float birthtime;
layout(location = LIFETIME_LOCATION) 	in float lifetime;

out vec2 		v_position;
flat out vec2 	v_moveVector;
flat out vec3 	v_color;
flat out float 	v_velocity;
flat out float 	v_pointSize;
flat out float 	v_birthtime;
flat out float 	v_lifetime;

// return random float value and update seed
float random(inout float seed) {
	float vertexId = float(gl_VertexID) / float(u_numParticles);
	vec2 texCoord = vec2(seed, vertexId);
	seed += 0.2;
	return texture(s_noiseMap, texCoord).r;
}

void main() {
	float seed = u_time;
	float timeleft = birthtime + lifetime - u_time;
	float halfWidth = u_canvas.x/two;
	float halfHeight = u_canvas.y/two;
	
	if (timeleft <= zero && random(seed) < u_emmissionRate) {
		// generate new particle
		v_position = vec2(random(seed) * u_canvas.x - halfWidth, 
							random(seed) * u_canvas.y - halfHeight);
		v_moveVector = normalize(vec2(random(seed) * two - one, 	
										random(seed) * two - one));
		v_color = vec3(random(seed), random(seed), random(seed));
		v_velocity = min_velosity + random(seed) * range_velosity;
		v_pointSize = min_size + random(seed) * range_size;
		v_birthtime = u_time;
		v_lifetime = min_lifetime + random(seed) * range_lifetime;		
	} else {
		// just copy old values
		v_position = position + moveVector * velocity;
		v_moveVector = moveVector;
		v_color = color;
		v_velocity = velocity;
		v_pointSize = pointSize;
		v_birthtime = birthtime;
		v_lifetime = lifetime;
		
	}
	
	float rad = (v_pointSize/two) * u_scale; // radius of ball
	
	if (((v_position.x - rad <= -halfWidth) && (v_moveVector.x < zero)) || 
		((v_position.x + rad >= halfWidth) && (v_moveVector.x > zero))) {
		// horizontal collision detected
		v_moveVector.x *= -one;
	}
	
	if (((v_position.y - rad <= -halfHeight) && (v_moveVector.y < zero)) ||
		((v_position.y + rad >= halfHeight) && (v_moveVector.y > zero))) {
		// vertical collision detected
		v_moveVector.y *= -one;
	}
}