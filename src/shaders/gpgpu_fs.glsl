#version 430 core
precision mediump float;

in vec2 		v_position;
flat in vec2 	v_moveVector;
flat in vec3 	v_color;
flat in float 	v_velocity;
flat in float 	v_pointSize;
flat in float 	v_birthtime;
flat in float 	v_lifetime;

out vec4 color;

void main() {
	color = vec4(1.0f);
}