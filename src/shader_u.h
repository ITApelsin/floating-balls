#ifndef FB_SHADER_U_H
#define FB_SHADER_U_H

#include <glad/glad.h>
#include <string>

namespace shader_utils
{
    /**
    *   Read text file to <dest>.
    *   If successful return true, otherwise return false.
    **/
    bool readFile(std::string& dest, const char* path);
    /**
    *   Compile Shader from <source> and write handler to <dest>.
    *   If successful return true, otherwise return false and write log to <errorLog> (if <errorLog> is not null)
	**/
    bool compileShaderFromSource(GLuint &dest, GLenum shaderType, const char* source, std::string *errorLog = nullptr);
    /**
    *   Read shader source from file <path>, compile shader and write handler to dest
    *   If successful return true, otherwise return false and write log to <errorLog> (if <errorLog> is not null).
    **/
    bool compileShaderFromFile(GLuint &dest, GLenum shaderType, const char * path, std::string *errorLog = nullptr);

    /**
    *   OpenGL programs builder
    **/
    class GLProgramBuilder
    {
        GLuint* vertexShader = nullptr;
        GLuint* fragmentShader = nullptr;
        bool autoDetaching = true;
        struct
        {
            GLsizei count = 0;
            const char** varyings = nullptr;
            GLenum bufferMode = GL_INTERLEAVED_ATTRIBS;
        } transformFeedback;
    public:
        /**
        *   Reset builder state.
        *   return pointer to this instance
        **/
        GLProgramBuilder* reset();
        /**
        *   Set Vertex Shader source and Fragment Shader source.
        *   return pointer to this instance
        **/
        GLProgramBuilder* setShaders(GLuint * const vertex_shader, GLuint * const fragments_shader);
        /**
        *  	If <autoDetaching> is true then the shaders will automatically detached after linking the program
        *   return pointer to this instance
        **/
        GLProgramBuilder* setAutoDetaching(bool autoDetaching = true);
        /**
        *   Configure transform feedback
		*	If <count> is not 0, then program will linked with transform feedback
        *   return pointer to this instance
        **/
        GLProgramBuilder* setTransformFeedback(GLsizei count, const char** varyings, GLenum bufferMode = GL_INTERLEAVED_ATTRIBS);
        /**
        *   Link OpenGL program and write handler to <dest>.
        *   If successful return true, otherwise return false and write log to <errorLog> (if <errorLog> is not null)
        **/
        bool link(GLuint &dest, std::string *errorLog = nullptr);
    };
}

#endif // FB_SHADER_U_H
