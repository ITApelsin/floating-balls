# Floating Balls

Floating Balls это небольшой OpenGL проект демонстрирующий эффект абсолютно упругого столкновения 2D окружностей.

##  Минимальные требования

- Видеокарта, поддерживающая OpenGL 4.3
- Windows x64
- MinGW64
- СMake 3.3+

## Сборка

Windows:

```bat
mkdir build
cd build
set PATH=%MinGW64_HOME%\bin;%PATH%
cmake -G "CodeBlocks - MinGW Makefiles" ../src
make
pause
```

